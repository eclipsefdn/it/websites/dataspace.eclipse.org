---
title: "Become a Member"
seo_title: "Become a Member | Eclipse Dataspace"
keywords: ["Eclipse Dataspace", "dataspace", "membership", "contact us"]
container: "container"
hide_sidebar: true
---

Is your organisation ready to become a member? 
**[Join now](https://membership.eclipse.org/application/ready-to-join)**.

If you have questions about membership in this working group or would like some
assistance with the application process, complete the form below.

{{< grid/div isMarkdown="false" class="margin-y-20" >}}
<h2 id="contact-us-about-membership">Contact Us About Membership</h2>
{{< hubspot_contact_form portalId="5413615" formId="d7156741-f0c8-47d9-9dcd-a9a08d82c328" >}}
{{</ grid/div >}}
