---
title: "Eclipse Dataspace"
date: 2022-09-15T10:00:00-04:00
headline: "Eclipse Dataspace Working Group"
links: [[href: "/become-a-member/", text: "Become a Member"], [href: "/about/", text: "Learn More"]]
jumbotron_class: "col-sm-12 col-sm-offset-6"
header_wrapper_class: "homepage"
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
layout: "single"
---

{{< pages/home/members_section >}}

{{< pages/home/news_events_section >}}
