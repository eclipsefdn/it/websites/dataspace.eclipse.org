---
title: "Projects"
seo_title: "Projects - Eclipse Dataspace"
headline: ""
description: ""
keywords: ["Eclipse Dataspace", "dataspace", "projects", "eclipse projects"]
header_wrapper_class: "projects"
layout: single
hide_page_title: true
hide_sidebar: true
---

# Projects { .text-center }

{{< grid/div class="row" isMarkdown="false" >}}
  <ul
    class="eclipsefdn-projects list-unstyled"
    data-url="https://projects.eclipse.org/api/projects?working_group=dataspace"
    data-classes="margin-top-30"
  ></ul>
{{</ grid/div >}}
