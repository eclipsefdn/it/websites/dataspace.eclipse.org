---
title: About the Working Group
seo_title: About the Working Group | Eclipse Dataspace
hide_sidebar: true
hide_page_title: true
---

# What is the Eclipse Dataspace Working Group? { .h1 .margin-bottom-30 .text-center }

The mission of the Eclipse Dataspace Working Group is to provide a forum for individuals and organisations to build and promote open source software, specifications, and open collaboration models needed to create scalable, modular, extensible, industry-ready open source components based on open standards for dataspaces.

The working group will focus on participating in standards development, implementation, and onboarding of existing open source projects including the Eclipse Dataspace Components project, and guiding associated projects in alignment with the overarching goal of supporting a broad ecosystem of interoperable dataspaces.

{{< grid/div class="text-center margin-top-40 margin-bottom-30" isMarkdown="false">}}

<a class="btn btn-primary" href="https://www.eclipse.org/org/workinggroups/dataspace-charter.php">Read the Charter</a>
<a class="btn btn-primary" href="/become-a-member/">Become a Member</a>

{{</ grid/div >}}
